#pragma once
#include <SFML/Graphics.hpp>
using sf::Vector2f;
using namespace std;
class Vec2f : public Vector2f {
public:
	Vec2f(float _x, float _y) :
		Vector2f(_x, _y) {}
	Vec2f( Vector2f const&p1,  Vector2f const&p2) :
		Vector2f(p2 - p1) {}
	Vec2f( Vector2f const&v) :
		Vector2f(v) {}

	Vec2f&	Normilize() {
		float modul = sqrt(x*x + y*y);
		*this /= modul;
		return *this;
	}
	Vec2f	GetUnitRep()const {
		float modul = sqrt(x*x + y*y);
		return *this / modul;
	}
	float Cross(Vec2f const &v) {
		return x*v.y - y*v.x;
	}
	Vec2f	GetNormal() const {
		return Vec2f(-y, x);
	}
	float	GetLength() const {
		return sqrt(x*x + y*y);
	}
	float	Dot( Vector2f const&v) const {
		return x*v.x + y*v.y;
	}
	float	GetAngel( Vector2f const&v) const {
		return acosf(
			Dot(v) /
			(GetLength() * Vec2f(v).GetLength())
		);
	}
	float	GetCosOfAngel( Vector2f const&v) const {
		return 	Dot(v) / (GetLength() * Vec2f(v).GetLength());
	}
private:
	operator sf::Vector2f() {
		//		cerr << "converted\n";
		return sf::Vector2f(x, y);
	}
};