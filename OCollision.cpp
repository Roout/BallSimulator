#include "OCollision.h"
#include "MyMath.h"
#include "Ball.h"



void OCollision::SolveBallCollision(shared_ptr<Ball> l, shared_ptr<Ball> r) {
	//calc angel between (velocity & x_axis).
	Vec2f	x_axis(r->mInfo->center - l->mInfo->center);
	float	cos_theta_lx = x_axis.GetCosOfAngel(l->mInfo->velocity),
			cos_theta_rx = x_axis.GetCosOfAngel(r->mInfo->velocity);
	//projection on x_axis
	float	v0x_l = (l->mInfo->velocity).GetLength()*cos_theta_lx,
			v0x_r = (r->mInfo->velocity).GetLength()*cos_theta_rx,
			dv_0x = v0x_l - v0x_r;
	/*
	System of equations:
	(1) mass_l * vfx_l + mass_r * vfx_r = v0x_l + v0x_r;
	(2) vfx_r - vfx_l = dv_0x
	Result:
	vfx_r = dv_0x + vfx_l;
	mass_l * vfx_l + mass_r * (dv_0x + vfx_l) = v0x_l + v0x_r;
	mass_l * vfx_l + mass_r * dv_0x + mass_r * vfx_l = v0x_l + v0x_r;

	vfx_l * (mass_l + mass_r) = v0x_l + v0x_r - mass_r * dv_0x;
	vfx_l = (v0x_l + v0x_r - mass_r * dv_0x) / (mass_l + mass_r);
	----------
	vfx_l = (v0x_l + v0x_r - mass_r * dv_0x) / (mass_l + mass_r);
	vfx_r = v0x_l - v0x_r - vfx_l;
	----------
	*/
	float	vfx_l = (v0x_l + v0x_r - r->mInfo->mass * dv_0x) / (l->mInfo->mass + r->mInfo->mass),
			vfx_r = dv_0x + vfx_l,
			vfy_l = (l->mInfo->velocity).GetLength() * sqrt(1 - cos_theta_lx*cos_theta_lx),
			vfy_r = (r->mInfo->velocity).GetLength() * sqrt(1 - cos_theta_rx*cos_theta_rx);
	//change velocity vector:
	l->mInfo->velocity = Vec2f(vfx_l, vfy_l);
	r->mInfo->velocity = Vec2f(vfx_r, vfy_r);
}

//check collision
OCollision::IType OCollision::CalcBallCollisionTime(shared_ptr<Ball> b1, shared_ptr<Ball> b2, float& time) {
	
	//try to solve equation of t:  sqrt(d(t))=r1+r2
	float	r = b1->mInfo->radius	+ b2->mInfo->radius;
	Vec2f	s = b1->mInfo->center	- b2->mInfo->center,
			v = b1->mInfo->velocity - b2->mInfo->velocity;
	//(v*v)*t^2 +2*(v*s)*t+(s*s-r*r)=0 
	float c = s.Dot(s) - r*r;
	//spheres overlaping
	if (c < 0.f) {
		time = 0.f;
		return IType::overlap;
	}
	float a = v.Dot(v); //a can be 0 only if v{0,0}
	if (a < EPS)	return IType::notCross; // not moving relative each other
	float b = v.Dot(s);
	if (b >= 0.f)	return IType::notCross;//balls not moving towards each other
	float d = b*b - a*c;
	if (d < 0.f)	return IType::notCross;//no real-valued root=>no intersection

	time = (-b - sqrt(d)) / a;

	return IType::cross;
}