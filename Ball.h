#pragma once
#include <memory>
#include "MyMath.h"



struct BallInfo {
	float		mass,
				radius;
	Vec2f		center,
				acceleration,
				velocity;
	BallInfo(
		float m,
		float r,
		Vec2f v,
		Vec2f a,
		Vec2f cent
	) :
		mass(m),
		radius(r),
		velocity(v),
		acceleration(a),
		center(cent)
	{}
};


class Ball {
public:
	friend class OCollision;
	Ball(const BallInfo&info) :
		mInfo(make_shared<BallInfo>(info))
	{}

private:
	shared_ptr<BallInfo> mInfo;
};


class Segment {
public:
	friend class OCollision;
	Segment(Vec2f p1, Vec2f p2) :
		pBeg(p1), pEnd(p2) {}

	Vec2f Dir()const { return Vec2f(pEnd.x-pBeg.x,pEnd.y-pEnd.y); }
	Vec2f Normal()const { return Dir().GetNormal(); }
private:
	Vec2f pBeg, pEnd;
};

