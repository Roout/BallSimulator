#pragma once
#include <memory>
#include "MyMath.h"


using std::shared_ptr;
class Ball;
class Segment;
class OCollision {
	enum class IntersectionType {
		notCross	= 0,
		cross		= 1,
		overlap		= 2,
		undefined	= 4
	};
	typedef IntersectionType IType;
	friend const IType operator& (IType const& lsh, IType const& rsh) {
		return static_cast<IType>(
			static_cast<int>(lsh)& static_cast<int>(rsh)
			);
	}
public:

	// can be called when two balls are already collide
	// only work with VELOCITY and it's DIRECTION
	// find new velocity' vector
	void SolveBallCollision(shared_ptr<Ball>l, shared_ptr<Ball>r);
	void SolveSegmentCollision(shared_ptr<Segment>l, shared_ptr<Ball>r) {

	}
private:
	const float EPS = 0.0000001f;
	//check collision
	IType CalcBallCollisionTime		(shared_ptr<Ball> b1,	shared_ptr<Ball> b2,	float& time);
	IType CalcSegmentCollisionTime	(shared_ptr<Segment> s,	shared_ptr<Ball> b,		float& time) {
				
	}
};