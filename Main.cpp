#include <iostream>
#include "Ball.h"
#include "OCollision.h"
#include "MyMath.h"


int main()
{

	/*BallInfo	a(3.5f, 2.f, 4.f, 0.f, Vector2f(0, 0), Vector2f( sqrtf(3),  1)),
				b(5.f,  3.f, 3.f, 0.f, Vector2f(5, 0), Vector2f(-sqrtf(2), -sqrtf(2)));
	shared_ptr<Ball>	A = make_shared<Ball>(a),
						B = make_shared<Ball>(b);
	OCollision col;
	col.SolveBallCollision(A, B);*/
	Vec2f	p1{ 0, 0 }, 
			p2{ 5, 5 }, 
			c0{ 4, 0 }, 
			a{ -1, 2 },
			l{ p2 - p1 },
			n{l.GetNormal()},
			v{ -1, 2 };

	float	r = 1.f, velocity = 1.f;

	v.Normilize();
	a.Normilize();
	v*=velocity;
	

	float	k1 = r*n.GetLength(),
			k2 = n.Dot(c0) - n.Dot(p1),
			k3 = n.Dot(v),
			k4 = n.Dot(a) / 2,
			k5 = k2 - k1,
			k6 = k1 + k2,
			D1=k3*k3-4*k4*k5,
			D2=k3*k3-4*k4*k6;

	if (D1 < 0 && D2 < 0) { 		
		//no real-valued root=> no intersection 	
		cout << "Ooops\n";
	} 	
	else if (D1 >= 0 && D2 < 0) { 		
		//1 or 2 roots from first equation => our time is the first positive one 	
		float	t1 = (-k3 + sqrtf(D1)) / (2.f * k4),
				t2 = (-k3 - sqrtf(D1)) / (2.f * k4);
		cout << "t1 = " << t1 << "\nt2 = " << t2 << endl;
	} 	
	else if (D1 < 0 && D2 >= 0) { 		
		//1 or 2 roots from second equation => our time is the first positive one 	
		float	t3 = (-k3 + sqrtf(D2)) / (2.f * k4),
				t4 = (-k3 - sqrtf(D2)) / (2.f * k4);
		cout << "t3 = " << t3 << "\nt4 = " << t4 << endl;
	} 	
	else { 		
		//2-4 roots from both equations => our time is the first positive one 	
		float	t1 = (-k3 + sqrtf(D1)) / (2.f * k4),
				t2 = (-k3 - sqrtf(D1)) / (2.f * k4);
		cout << "t1 = " << t1 << "\nt2 = " << t2 << endl;
		float	t3 = (-k3 + sqrtf(D2)) / (2.f * k4),
				t4 = (-k3 - sqrtf(D2)) / (2.f * k4);
		cout << "t3 = " << t3 << "\nt4 = " << t4 << endl;
	}

	cin.get();
	cin.get();
	return 0;
}

/*
IntersectionType TestSegmentsIntersection(
const Vec2f&s1,
const Vec2f&f1,
const Vec2f&s2,
const Vec2f&f2)
{

const Vec2f v1 = Vec2f(f1 - s1).Normilize();
float	fn1	= v1.y*(s2.x - s1.x) - v1.x*(s2.y - s1.y),
fn2	= v1.y*(f2.x  - s1.x) - v1.x*(f2.y  - s1.y);

IType type = IType::undefined;

//if signs are same
if ((fn1 >=  EPS && fn2 >=  EPS) ||
(fn1 <= -EPS && fn2 <= -EPS))
{
type = IType::notCrossing;
}
//signs are different
else if ((fn1 >=  EPS && fn2 <= -EPS) ||
(fn1 <= -EPS && fn2 >=  EPS))
{
type = IType::crossing;
}
//both are  zero
else if ((fn1 + EPS > 0 && fn1 - EPS < 0) &&
(fn2 + EPS > 0 && fn2 - EPS < 0))
{
type = IType::overlap;
}
//zero & not zero
else
{
type = IType::crossing;
}

return type;
}
*/